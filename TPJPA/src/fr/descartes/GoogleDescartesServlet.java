package fr.descartes;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;

@SuppressWarnings("serial")
public class GoogleDescartesServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("MIAGE DESCARTES");
		
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		
		Entity etudiant = new Entity("Etudiant");
		etudiant.setProperty("firstName", "Oussama"); 
		etudiant.setProperty("lastName", "AitDaoud"); 
		etudiant.setProperty("hireDate", new Date()); 
		etudiant.setProperty("attendedHrTraining", true);
		datastore.put(etudiant);

		
		Entity professeur = new Entity("Professeur");
		professeur.setProperty("firstName", "Benoit"); 
		professeur.setProperty("lastName", "Charroux"); 
		professeur.setProperty("hireDate", new Date()); 
		professeur.setProperty("attendedHrTraining", true);
		datastore.put(professeur);
		
		String firstName="Oussama";
		Filter propertyFilter = new FilterPredicate("firstName", FilterOperator.EQUAL, firstName);
		Query q = new Query("Etudiant").setFilter(propertyFilter); 
		List<Entity> results = datastore.prepare(q.setKeysOnly()). asList(FetchOptions.Builder.withDefaults());
		Query q1 = new Query("Person").addSort("lastName", SortDirection.ASCENDING);
		
		System.out.println(results.toString());
	}
}
