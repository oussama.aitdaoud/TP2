package test.com.univ;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.univ.Car;
import com.univ.Person;

/**
 * Hello world!
 *
 */
@SuppressWarnings("unused")
public class App 
{
    public static void main( String[] args )
    {
    	EntityManagerFactory emf = Persistence.createEntityManagerFactory("manager1");
		EntityManager entityManager = emf.createEntityManager();
		
		EntityTransaction tx = entityManager.getTransaction();
		
    	try{
    		
			tx.begin();
			
			//Person p = new Person();
			//p.setName("Tintin");
					
			//DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			//Date beginDate = dateFormat.parse("23/09/2015");
			//p.setDate(beginDate);
			
			//entityManager.persist(p);
			
			Car c = new Car();
			c.setNumberOfSeats(5);
					
			c.setPlatenumber(234);
			
			entityManager.persist(c);
				
			tx.commit();			
		
		}catch(Exception e){
			tx.rollback();
		}
		
	}
}
